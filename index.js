class Emplooyee {
  constructor({ name, age, salary } = {}) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(newName) {
    this._name = newName;
  }
  get age() {
    return this._age;
  }
  set age(newAge) {
    this._age = newAge;
  }
  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
}

const emplooyee = new Emplooyee({
  name: "Alisa",
  age: 18,
  salary: 25000,
});

class Programmer extends Emplooyee {
  constructor({ name, age, salary, lang } = {}) {
    super({ name, age, salary });
    this._lang = lang;
  }

  set salary(newSalary) {
    this._salary = newSalary * 3;
  }
}

const programmer = new Programmer({
  name: "Maks",
  age: 20,
  salary: 25000,
  lang: ["English", "French", "Ukrainian"],
});

const programmer1 = new Programmer({
    name: "Karina",
    age: 22,
    salary: 200,
    lang: ["English", "German", "Ukrainian"],
  });

  const programmer2 = new Programmer({
    name: "Dima",
    age: 24,
    salary: 4000,
    lang: ["German", "Ukrainian"],
  });


programmer.salary = 3000;
console.log(programmer);
console.log(programmer1);
console.log(programmer2);

console.log(emplooyee);
